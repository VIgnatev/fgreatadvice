//
//  ViewController.swift
//  fGreatAdvice
//
//  Created by Владислав Игнатьев on 08.12.2017.
//  Copyright © 2017 Владислав Игнатьев. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class ViewController: UIViewController {
	@IBOutlet weak var advice: UITextView!
	@IBAction func generateNewAdvice(_ sender: UIButton) {
    // getDataObjectMapper()
    getDataCodable()
	}

  let url = "http://fucking-great-advice.ru/api/random/"
	
	override func viewDidLoad() {
		super.viewDidLoad()

    // getDataObjectMapper()
    getDataCodable()
	}
	
	private func getDataObjectMapper() {
		let url = "http://fucking-great-advice.ru/api/random/"
		
		Alamofire.request(url, method: .get).validate().responseJSON { response in
			if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
				let rawModels = try? Mapper<JsonModel>().map(JSONString: utf8Text)
				guard let models = rawModels else {
					return
				}

				self.advice.text = models.text
			}
		}
	}
  
  private func getDataCodable() {
    let url = "http://fucking-great-advice.ru/api/random/"

    Alamofire.request(url, method: .get).validate().responseJSON { response in
      if let data = response.data {
        let jsonDecoder = JSONDecoder()

        let rawContent = try? jsonDecoder.decode(CodableJsonModel.self, from: data)
        guard let content = rawContent else {
          return
        }

        self.advice.text = content.text
      }
    }
  }
}
