//
//  JsonModel.swift
//  fGreatAdvice
//
//  Created by Владислав Игнатьев on 08.12.2017.
//  Copyright © 2017 Владислав Игнатьев. All rights reserved.
//

import ObjectMapper

struct JsonModel: ImmutableMappable {
	let id  : Int64
	let text: String
	
	init(map: Map) throws {
		id   = try map.value("id")
		text = try map.value("text")
	}
}
