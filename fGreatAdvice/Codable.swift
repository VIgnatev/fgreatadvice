//
//  Codable.swift
//  fGreatAdvice
//
//  Created by Владислав Игнатьев on 27.01.2018.
//  Copyright © 2018 Владислав Игнатьев. All rights reserved.
//

import Foundation

struct CodableJsonModel: Codable {
  var id  : Int64
  var text: String
}
